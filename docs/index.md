# Python 예외 처리: Try, Except, Finally <sup>[1](#footnote_1)</sup>

> <font size="3">Python의 내장 함수를 사용하여 파일을 읽고 쓰고 추가하는 방법에 대해 알아본다.</font>

## 목차

1. [들어가며](./exception-handling.md#intro)
1. [Python의 오류와 예외란?](./exception-handling.md#sec_02)
1. [`raise` 키워드를 사용하여 예외를 제기하는 방법](./exception-handling.md#sec_03)
1. [`try`와 `except` 블록을 사용하여 예외를 처리하는 방법](./exception-handling.md#sec_04)
1. [`try`와 `except`와 같이 `else`와 `fillay` 블록을 사용하는 방법](./exception-handling.md#sec_05)
1. [`Exception` 클래스를 사용하여 커스텀 예외를 만드는 방법](./exception-handling.md#sec_06)
1. [요약](./exception-handling.md#summary)


<a name="footnote_1">1</a>: [Python Tutorial 18 — Python Exception Handling: Try, Except, Finally](https://python.plainenglish.io/python-tutorial-18-python-exception-handling-try-except-finally-ff21d6e589fc?sk=a6ad6b616e1ac17f36143b4f7343dccf)를 편역한 것이다.
