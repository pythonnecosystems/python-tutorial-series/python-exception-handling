# Python 예외 처리: Try, Except, Finally

## <a name="intro"></a> 들어가며
이 포스팅에서는 Python에서 `try`, `except`와 `finally` 블럭을 사용하여 오류와 예외를 처리하는 방법을 다룰 것이다.

오류와 예외는 어떤 프로그래밍 언어에서든지 피할 수 없다. 구문 오류, 논리적 오류 또는 예기치 않은 이벤트 같이 프로그램 실행 중에 무엇인가가 잘못되었을 때 발생한다. 오류와 예외는 프로그램을 충돌시키거나 예기치 않게 행동하게 하여 바람직하지 않은 결과를 초래하거나 사용자의 좌절을 초래할 수 있다.

다행스럽게도 Python은 프로그램을 갑자기 종료하지 않고도 오류와 예외를 우아하게 처리할 수 있는 방법을 제공한다. 오류나 예외가 발생했을 때 어떤 조치를 취해야 하는지, 오류나 예외 발생 여부와 상관없이 어떤 조치를 취해야 하는지를 `try`, `except`와 `finally` 블럭을 사용하여 이를 수행한다.

`try`, `except`와 `finally` 블럭을 사용하여 Python 프로그램의 신뢰성, 견고성 및 사용자 친화성을 향상시킬 수 있다. 또한 커스텀 예외를 생성하여 내장 예외에서 다루지 않는 특정 상황을 처리할 수 있다.

이 글으로 다음과 같은 내용을 배울 수 있다.

- Python의 오류와 예외는 무엇일까?
- `raise` 키워드를 사용하여 어떻게 예외를 제기할 수 있을까?
- `try`와 `except` 블록을 사용하여 어떻게 예외를 처리할 수 있을까?
- 어떻게 `try`와 `except`와 같이 `else`와 `fillay` 블록을 사용할 수 있을까?
- `Exception` 클래스를 사용하여 커스텀 예외를 어떻게 만들 수 있을까?

이 글을 통하여 Python의 오류와 예외를 효과적이고 우아하게 처리할 수 있을 것이다. 시작하겠다!

## <a name="sec_02"></a> Python의 오류와 예외란?
Python에서 오류와 예외를 다루는 방법을 배우기 전에 먼저 그것들은 무엇인지, 또한 어떻게 다른지 이해해보자.

오류는 프로그램의 구문이나 논리에서 발생하는 오류로 프로그램이 실행되거나 올바른 출력을 내는 것을 방해한다. 예를 들어 괄호를 닫는 것을 잊거나 0으로 나누려고 하면 오류가 생긴다. **오류는 프로그램 실행 전 또는 실행 중에 Python 인터프리터에 의해 탐지되어 프로그램이 중지되거나 종료된다**.

**예외는 프로그램의 실행 중에 발생하는 이벤트로서 프로그램의 정상적인 흐름을 방해한다**. 예를 들어, 존재하지 않는 파일을 열려고 하거나 범위를 벗어난 요소에 접근하려고 하면 예외가 발생한다. Python 런타임 시스템이 처리할 수 없는 상황에 직면했을 때 예외가 제기된다. 예외를`try`, `except`와 `finally` 블럭을 사용하여 프로그램에서 처리할 수 있으며, 이는 [다음 절](#sec_03)에서 다룰 것이다.

Python에는 여러 종류의 오류와 예외가 존재하며, 그 성격과 원인에 따라 서로 다른 범주로 분류된다. 일반적인 범주로는 다음과 같은 것들이 있다.

- **`SyntaxError`**: 프로그램에서 구문 오류가 발생했을 때 Python 인터프리터가 일으킨다. 예를 들어 `for` 루프 뒤에 콜론이 없으면 `SyntaxError`가 발생한다.
- **`ZeroDivisionError`**: 어떤 수를 0으로 나누려고 할 때 발생한다. 예를 들어, `1/0`에서 `ZeroDivisionError`가 발생한다.
- **`NameError`**: 변수나 함수가 정의되지 않았거나 범위에 포함되지 않은 경우에 발생한다. 예를 들어 `x`를 정의하지 않고 `print(x)`를 만나면 `NameError`가 나온다.
- **`TypeError`**: 부적절한 유형의 객체에 대해 연산이나 함수를 수행하려고 할 때 발생한다. 예를 들어 문자열과 숫자를 더하려고 하면 `TypeError`가 발생한다.
- **`ValueError`**: 주어진 함수나 연산에 적합하지 않은 값을 사용하려고 할 때 발생한다. 예를 들어 숫자가 아닌 문자열을 정수로 변환하려고 하면 `ValueError`가 나타난다.
- **`IndexError`**: 리스트나 튜플과 같이 범위를 벗어난 요소를 액세스하려고 하면 이 문제가 발생한다. 예를 들어, 4개의 요소만 있는 리스트에서 다섯 번째 요소를 액세스하려고 하면 `IndexError`가 발생한다.
- **`KeyError`**: 사전에 없는 키를 액세스하려고 할 때 발생한다. 예를 들어 사전에 없는 키의 값을 액세스하려고 하면 `KeyError`가 발생한다.
- **`FileNotFoundError`**: 존재하지 않거나 액세스 권한이 없는 파일을 열려고 할 때 발생한다. 예를 들어 현재 디렉터리에 없는 파일을 열려고 하면 `FileNotFoundError`가 발생한다.

[여기](https://docs.python.org/3/library/exceptions.html)에서 Python 내장 오류와 예외의 전체 목록을 볼 수 있다.

이제 Python에서 어떤 오류와 예외가 있는지 알았으니, [다음 절](#sec_03)에서 `raise` 키워드를 사용하여 예외를 제기하는 방법을 알아보자.

## <a name="sec_03"></a> `raise` 키워드를 사용하여 예외를 제기하는 방법
어떤 상황에서는 프로그램에서 의도적으로 예외를 제기하고 싶을 수도 있다. 예를 들어, 입력의 유효성을 확인하거나 특정 조건을 적용하거나 프로그램의 다른 부분에 오류를 시그널링하고 싶을 수 있다. 이를 위해 Python에서 `raise` 키워드를 사용할 수 있다.

`raise` 키워드를 사용하면 예외 객체를 생성하여 오류를 만들 수 있다. 예외의 유형과 메시지를 지정하거나 기존의 예외 객체를 사용할 수 있다. 예를 들어 다음과 같이 작성할 수 있다.

```python
# Raise a ValueError with a custom message
raise ValueError("Invalid input")
```

```python
# Raise a ZeroDivisionError using an existing exception object
x = 0
e = ZeroDivisionError("Cannot divide by zero")
if x == 0:
    raise e
```

예외를 제기하면 프로그램의 정상적인 흐름이 중단되고 Python 인터프리터는 예외를 처리할 방법을 찾는다. 예외에 대한 핸들러가 없으면 프로그램은 종료되고 예외 메시지가 표시된다. 예를 들어 다음 코드를 실행하면 `ValueError`가 나타난다.

```python
# Ask the user to enter a positive number
num = int(input("Enter a positive number: "))

# Raise a ValueError if the number is negative
if num < 0:
    raise ValueError("The number must be positive")
```

출력:

```python
Enter a positive number: -5
Traceback (most recent call last):
  File "main.py", line 5, in 
    raise ValueError("The number must be positive")
ValueError: The number must be positive
```

그러나 예외를 처리하고 프로그램이 충돌하는 것을 방지하려면 `try`와 `except` 블록을 사용할 수 있으며 [다음 절](#sec_04)에서 설명한다.

## <a name="sec_04"></a> `try`와 `except` 블록을 사용하여 예외를 처리하는 방법
이제 `raise` 키워드를 사용하여 예외를 발생시키는 방법을 알았으므로 Python에서 `try`와 `except` 블록을 사용하여 예외를 처리하는 방법을 살펴보자.

`try`와 `except` 블록은 예외를 발생시킬 수 있는 코드 블록을 둘러싸는 데 사용되며, 예외가 발생했을 때 어떤 조치를 취할 것인지 지정한다. `try`와 `except` 블록의 구문은 다음과 같다:

```python
try:
    # Code that may raise an exception
except ExceptionType as e:
    # Code that handles the exception
```

`try` 블록은 예외를 발생시킬 수 있는 코드를 포함한다. `except` 블록은 예외를 처리하는 코드를 포함한다. `ExceptionType`은 `ValueError`, `ZeroDivisionError` 등과 같이 처리하고자 하는 예외 타입이다. `e`는 예외 객체를 지정하는 변수로 예외 메시지나 다른 속성을 액세스하는 데 사용할 수 있다.

`try` 블록이 실행되면 다음과 같은 시나리오 중 하나가 발생할 수 있다.

- 예외가 발생하지 않으면 `try` 블록이 완료되고 예외 블록은 건너뛴다.
- `ExceptionType`과 일치하는 예외가 발생하면 `try` 블록이 중지되고 `except` 블록을 실행한다.
- `ExceptionType`과 일치하지 않는 예외가 발생하면 `try` 블록이 중지되고 예외가 외부 스코프 또는 Python 인터프리터로 전파된다.

예를 들어, 어떤 수의 역수를 계산하는 함수가 있고, 그 수가 `0`일 때 발생할 수 있는 `ZeroDivisionError`를 처리하려고 한다고 가정하자.

```python
# Define a function that calculates the reciprocal of a number
def reciprocal(num):
    try:
        # Try to calculate the reciprocal
        result = 1 / num
        print(f"The reciprocal of {num} is {result}")
    except ZeroDivisionError as e:
        # Handle the ZeroDivisionError
        print(f"Cannot calculate the reciprocal of {num}")
        print(f"Exception: {e}")
```

만약 0이 아닌 2를 인수로 함수를 호출한다면, 예상대로 역수를 얻게 될 것이다.

```python
# Call the function with a non-zero number
reciprocal(2)
```

출력:

```python
The reciprocal of 2 is 0.5
```

인수 0으로 함수를 호출하면 `except` 블록이 처리하는 `ZeroDivisionError`를 얻는다.

```python
# Call the function with a zero number
reciprocal(0)
```

출력:

```
Cannot calculate the reciprocal of 0
Exception: division by zero
```

문자열과 같이 잘못된 입력을 가진 함수를 호출하면 예외 블록에서 처리되지 않는 다른 예외가 발생하고 프로그램이 종료된다.

```python
# Call the function with an invalid input
reciprocal("a")
```

출력:

```python
Traceback (most recent call last):
  File "main.py", line 13, in 
    reciprocal("a")
  File "main.py", line 4, in reciprocal
    result = 1 / num
TypeError: unsupported operand type(s) for /: 'int' and 'str'
```

보다시피, `try`와 `except` 블록을 사용하면 특정 예외를 처리하고 프로그램이 충돌하는 것을 방지할 수 있다. 그러나 예외가 발생하는지 여부에 관계없이 일부 코드를 실행하거나 예외가 발생하지 않을 때만 실행하기를 원할 수도 있다. 이를 위해 `else`와 `finally` 블록을 사용할 수 있으며, 이에 대해서는 [다음 절](#sec_05)에서 살펴보자.

## <a name="sec_05"></a> `try`와 `except`와 같이 `else`와 `finally` 블록을 사용하는 방법
[이전 절](#sec_04)에서는 Python에서 `try`와 `except` 블록을 사용하여 예외를 처리하는 방법을 배웠다. 이 절에서는 예외 발생 여부와 관계없이 또는 예외가 발생하지 않을 때만 `try`와 `except`와 힘께 `else`와 `finally` 블록을 사용하는 방법을 배울 것이다.

`else`와 `finally` 블록은 `try`와 `except`블록 후에 추가할 수 있는 선택적 블록이다. `try`, `except`, `else`와 `finally` 블록의  구문은 다음과 같다.

```python
try:
    # Code that may raise an exception
except ExceptionType as e:
    # Code that handles the exception
else:
    # Code that executes only if no exception occurs
finally:
    # Code that executes regardless of whether an exception occurs or not
```

`else` 블록은 `try` 블록에서 예외가 발생하지 않는 경우에만 실행하는 코드를 포함한다. `finally` 블록은 `try` 블록에서 예외가 발생하는지 여부와 관계없이 실행하는 코드를 포함한다. `else`와 `finally` 블록은 `try`와 `except`블록 실행 후에 어떤 정리 또는 최종 동작들을 수행하는 데 유용하다.

예를 들어, 파일을 열고 그 내용을 읽고 화면에 출력하는 함수를 가지고 있다고 가정해 보자. 파일이 존재하지 않을 경우 발생할 수 있는 `FileNotFoundError`를 처리하고 파일을 읽은 후에는 파일을 닫으려고 한다.

```python
# Define a function that opens a file, reads its content, and prints it to the screen
def read_file(filename):
    try:
        # Try to open the file
        file = open(filename, "r")
        # Read the file content
        content = file.read()
        # Print the file content
        print(content)
    except FileNotFoundError as e:
        # Handle the FileNotFoundError
        print(f"Cannot open the file {filename}")
        print(f"Exception: {e}")
    else:
        # Execute only if no exception occurs
        print(f"Successfully read the file {filename}")
    finally:
        # Execute regardless of whether an exception occurs or not
        # Check if the file is open
        if file:
            # Close the file
            file.close()
            print(f"Closed the file {filename}")
```

"test.txt"와 같이 유효한 파일 이름을 가진 함수를 호출하면 파일 내용이 화면에 인쇄되고 파일이 바르게 닫힌다.

```python
# Call the function with a valid filename
read_file("test.txt")
```

출력:

```
This is a test file.
Successfully read the file test.txt
Closed the file test.txt
```

`no_such_file.txt`와 같이 잘못된 파일 이름을 가진 함수를 호출하면 `except` 블록에서 `FileNotFoundError`가 처리되고 파일이 제대로 닫힌다.

```python
# Call the function with an invalid filename
read_file("no_such_file.txt")
```

출력:

```
Cannot open the file no_such_file.txt
Exception: [Errno 2] No such file or directory: 'no_such_file.txt'
Closed the file no_such_file.txt
```

보다시피 `else`와 `finally` 블록은 예외가 발생하지 않을 때만, 혹은 예외 발생 여부와 상관없이 어떤 코드를 실행할 수 있게 해준다. 이를 통해 프로그램의 적절한 기능과 종료를 보장하는 데 도움이 될 수 있다.

다음 절에서는 Python의 `Exception` 클래스를 사용하여 커스텀 예외를 만드는 방법을 다룰 것이다.

## <a name="sec_06"></a> `Exception` 클래스를 사용하여 커스텀 예외를 만드는 방법
[이전 절](#sec_05)에서는 Python에서 `try`, `except`, `else`와 `finally` 블록을 사용하여 내장 예외를 처리하는 방법을 배웠다. 그러나 때로는 내장 예외에서 다루지 않는 특정 상황을 처리하기 위해 커스텀 예외를 만들고자 할 필요가 있다. 예를 들어 잘못된 암호, 잔액 부족, 재고 부족 등에 대한 예외를 생성할 수 있다. 이를 위해 Python에서 `Exception` 클래스를 사용할 수 있다.

`Exception` 클래스는 Python의 모든 내장 예외에 대한 베이스(base) 클래스이다. 이 클래스에서 상속하여 커스텀 예외 클래스를 만들 수 있다. 커스텀 예외 클래스를 만드는 구문은 다음과 같다.

```python
# Define a custom exception class
class CustomException(Exception):
    # Define the constructor
    def __init__(self, message):
        # Call the base class constructor
        super().__init__(message)
        # Add any custom attributes
        self.custom_attribute = "some value"
```

`CustomException` 클래스는 `Exception` 클래스에서 상속되며 자체 생성자를 정의한다. 생성자는 메시지를 매개 변수로 사용하고 메시지를 사용하여 기저 클래스 생성자를 호출한다. 또한 일반 클래스와 마찬가지로 커스텀 예외 클래스에 커스텀 속성 또는 메서드를 추가할 수 있다.

커스텀 예외 클래스를 정의한 후 프로그램에서 예외를 일으키고 처리하는 데 사용할 수 있다. 예를 들어, 사용자의 비밀번호를 검사하는 기능이 있는데, 비밀번호가 올바르지 않은 경우 커스텀 예외를 일으키고자 한다고 가정해 보겠다. 다음과 같이 작성할 수 있다.

```python
# Define a custom exception class for invalid password
class InvalidPasswordException(Exception):
    # Define the constructor
    def __init__(self, message):
        # Call the base class constructor
        super().__init__(message)
```

```python
# Define a function that checks the password of a user
def check_password(user, password):
    # Assume the correct password is "secret"
    correct_password = "secret"
    # Compare the password with the correct password
    if password == correct_password:
        # Return True if the password is correct
        return True
    else:
        # Raise a custom exception if the password is incorrect
        raise InvalidPasswordException("The password is incorrect")
```

"secret"과 같이 올바른 암호로 함수를 호출하면 반환 값으로 `True`를 출력한다.

```python
# Call the function with a correct password
result = check_password("Alice", "secret")
print(result)
```

출력:

```
True
```

"wrong"과 같이 잘못된 암호로 함수를 호출하면 함수에 의해 커스텀 예외가 발생한다.

```python
# Call the function with an incorrect password
result = check_password("Alice", "wrong")
print(result)
```

출력:

```python
Traceback (most recent call last):
  File "main.py", line 21, in 
    result = check_password("Alice", "wrong")
  File "main.py", line 15, in check_password
    raise InvalidPasswordException("The password is incorrect")
InvalidPasswordException: The password is incorrect
```

또한 `try`와 `except` 블럭을 사용하여 내장 예외와 마찬가지로 커스텀 예외를 처리할 수 있다. 예를 들어 다음과 같이 작성할 수 있다.

```python
# Try to call the function with an incorrect password
try:
    result = check_password("Alice", "wrong")
    print(result)
except InvalidPasswordException as e:
    # Handle the custom exception
    print(f"An error occurred: {e}")
```

출력:

```python
An error occurred: The password is incorrect
```

보다시피 Python에서 커스텀 예외를 생성하고 사용하는 것은 쉽고 편리하다. 이를 사용하여 내장 예외에서 다루지 않는 특정 상황을 처리하고 프로그램을 보다 강력하고 사용자 친화적으로 만들 수 있다.

다음 [마지막 섹션](#conclusion)에서는 Python 예외 처리에 대하여 요약하고, 이 포스팅을 마무리한다.

## <a name="summary"></a> 요약
Python 예외 처리에 대하여 설명하했다. 여기에서는 Python에서 `try`, `except`와 `finally` 블록을 사용하여 오류와 예외를 처리하는 방법을 배웠다. 또한 `raise` 키워드를 사용하여 예외를 일으키는 방법과 `Exception` 클래스를 사용하여 커스텀 예외를 만드는 방법도 배웠다.

이러한 기술을 사용하면 Python 프로그램의 신뢰성, 견고성 및 사용자 친화성을 향상시킬 수 있다. 또한 내장 예외 사항에서 다루지 않는 특정 상황을 처리하고 프로그램을 보다 유연하고 적응할 수 있도록 할 수 있다.

다음은 이 포스팅에서 기억해야 할 핵심 사항이다.

- 어떤 프로그래밍 언어에서도 오류와 예외는 피할 수 없는 것이다. 구문 오류, 논리적 오류 또는 예기치 못한 이벤트와 같이 오류와 예외는 프로그램 실행 중에 무엇인가가 잘못되었을 때 발생한다.
- Python은 프로그램을 갑자기 종료하지 않고도 오류와 예외를 우아하게 처리할 수 있는 방법을 제공한다. 이는 오류나 예외가 발생했을 때 어떤 조치를 취해야 하는지, 오류나 예외 발생 여부와 상관없이 어떤 조치를 취해야 하는지를 지정할 수 있는 `try`, `except`와 `finally` 블록을 수행함으로써 이루어진다.
- `raise` 키워드를 사용하여 프로그램에서 의도적으로 예외를 제기할 수 있다. 예외의 타입과 메시지를 지정하거나 기존의 예외 객체를 사용할 수 있다.
- 내장 예외에서 다루지 않는 특정 상황을 처리하기 위해 자신만의 커스텀 예외를 만들 수 있다. `Exception` 클래스에서 상속하여 자신만의 커스텀 예외 클래스를 만들고 커스텀 속성 또는 메서드를 추가할 수 있다.

이에 대하여 보다 자세한 것을 원하면 [전문가를 위한 Python 예외와 오류 처리: FastAPI에서의 가이드](https://pythonnecosystems.gitlab.io/exception-error-handling)이 참고가 뵐 것입니다.

